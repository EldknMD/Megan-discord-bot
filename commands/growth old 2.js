const growthCooldown = {};
exports.run = async (client, message, args, sql, Discord) => {
	const timeDiff = growthCooldown[message.author.id] - new Date().getTime();
	if(timeDiff > 3600000){ //More than an hour left.
		const hours = Math.round((timeDiff /360000)) / 10
        message.channel.send("I'm feeling bad :c, let me rest for... maybe " + hours + " hours? :3 - " + message.author);
	} else if (timeDiff > 0) {
		const minutes = Math.round((timeDiff /6000)) / 10
        message.channel.send("I'm feeling bad :c, let me rest for... maybe " + minutes + " minutes? :3 - " + message.author);
    } else {
		var level = await(async function(){
			try{
				const member = message.guild.member(message.mentions.users.first());
				if(!member){
					const iUser = await sql.get(`SELECT * FROM userScores WHERE guildID = '${message.guild.id}' AND userID = '${member? member.id : message.author.id}'`); //gets user row of whos requesting
					if(!iUser){
						return 1;
					}else{
						return iUser.uLevel;
					}
				}
			} catch (e){
				console.log("Exception occured", e);
				return 1;
			}
		}(message));
		if(level && level < 1) {
			level = 1;
		}
		console.log("Level is:: "+level);
		const weightedRandom = function(level){
			var result = 0;
			for (let i=0; i<=level; i++){
				result = Math.max(Math.random(), result);
			}
			return result;
		}
        var fs = require("fs");
        var alturarose = fs.readFileSync("alturamegan.txt", "utf8");
        var hipModifier = parseFloat(fs.readFileSync("hipModifier.txt", "utf8"));
        var numberalturarose = Number(alturarose);
        var adicion = [3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 8, 8, 8, 8, 8, 10, 10, 10, 10, 20, 44, 100];
        var hipBonus = Math.random() < 0.075;
        if (hipBonus) {
            hipModifier += 0.0025;
        }
        var adicionrandom = Math.floor(Math.random() * adicion.length);
        var alturanuevarose = numberalturarose + adicion[adicionrandom];
        var bustorose = Math.round((alturanuevarose * 0.4)) / 100;
        var cinturarose = Math.round((alturanuevarose * 0.35)) / 100;
        var caderarose = Math.round((alturanuevarose * hipModifier)) / 100;
        var pesobustorose = Math.round(bustorose * 10);
        var alturaroseenmetros1 = alturarose / 100;
        var alturaroseenmetros2 = alturanuevarose / 100;
        var role = message.guild.roles.find(role => role.name === "Growth master");
        var adiciontotal = adicion[adicionrandom];
        fs.writeFile("alturamegan.txt", alturanuevarose, function(err) {
            if (err) return console.log(err)
        });
        fs.writeFile("hipModifier.txt", hipModifier, function(err) {
            if (err) return console.log(err);
        });
        var facts = [
            "Big in Japan!!!",
            "Get off my way! I need to see Jack now",
            "Who gave you that damn serum? It's... for a friend",
            "Gigantify!",
            "<<<<STOP!!!",
            "This feels weird...",
            "It went to my hips, isn't it?",
            "Don't make me step on you!",
            "Am I bigger than rose?",
            "I loved that jeans...",
            message.author + " STOP!!!"
        ];
        var fact = Math.floor(Math.random() * facts.length);
        var embed = new Discord.RichEmbed()
            .setTitle(alturaroseenmetros1 + " Meters" + "->" + alturaroseenmetros2 + " Meters")
            .setDescription(facts[fact])
            .setColor(0xCC7722)
            .addField("**Bust =" + bustorose + " Meters" + ", Waist =" + cinturarose + " Meters" + ", Hips=" + caderarose + " Meters**", "I'm huge >_<")
            .setThumbnail("http://i66.tinypic.com/4ku0yp.jpg");
        message.channel.send({
            embed
        });
        if (adiciontotal > 99) {
            message.member.addRole(role);
            message.channel.send(message.author + " You made me a monster! I'm one meter taller now T_T")
        }
        if (hipBonus) {
            message.channel.send(message.author + " Now where am I going to get a belt that fits? T_T");
        }
    }
    talkedRecently.add(message.author.id);
    setTimeout(() => {
        // Removes the user from the set after a minute
        talkedRecently.delete(message.author.id);
    }, 14400000);
};