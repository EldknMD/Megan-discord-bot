const talkedRecently = new Set();
exports.run = (client, message, args, sql, Discord) => {
    if (!talkedRecently.has(message.author.id)) {
        var fs = require("fs");
        var hipModifier = parseFloat(fs.readFileSync("hipModifier.txt", "utf8"));
        var numberalturarose = Number(fs.readFileSync("alturamegan.txt", "utf8"));
        var meterHeight = Math.round(numberalturarose) / 100;
        var bustorose = Math.round((numberalturarose * 0.4)) / 100;
        var cinturarose = Math.round((numberalturarose * 0.35)) / 100;
        var caderarose = Math.round((numberalturarose * hipModifier)) / 100;
        var facts = [
            "Let's see if Jack notices me now",
            "And Rose's still bigger than me",
            "Thanks god it stopped",
            "Are you alright down there?",
            "I was getting used to grow :c",
            "Bring me the big troublemaker now",
            "Mine were huge already",
            "Get out of mi sight",
            "This was a Rose's idea, isn't it?",
            "I'm gonna need one BIG towel to cover me",
            message.author + " this is your fault!!!"
        ];
        var fact = Math.floor(Math.random() * facts.length);
        var embed = new Discord.RichEmbed()
		
			.setTitle(meterHeight + " Meters")
            .setDescription(facts[fact])
            .setColor(0xCC7722)
            .addField("**Bust =" + bustorose + " Meters" + ", Waist =" + cinturarose + " Meters" + ", Hips=" + caderarose + " Meters**", "I'm huge >_<")
            .setThumbnail("http://i66.tinypic.com/4ku0yp.jpg");
        message.channel.send({
            embed
        });
    }
    talkedRecently.add(message.author.id);
    setTimeout(() => {
        // Removes the user from the set after a minute
        talkedRecently.delete(message.author.id);
    }, 1);
};