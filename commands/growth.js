const growthCooldown = {};
exports.run = async (client, message, args, sql, Discord) => {
	const timeDiff = growthCooldown[message.author.id] - new Date().getTime();
	if(timeDiff > 3600000){ //More than an hour left.
		const hours = Math.round((timeDiff /360000)) / 10;
        message.channel.send("I'm feeling bad, you prick, let me rest for maybe " + hours + " hours? >:C - " + message.author);
	} else if (timeDiff > 0) {
		const minutes = Math.round((timeDiff /6000)) / 10
        message.channel.send("I'm starting to feel better, let me rest for... maybe " + minutes + " minutes? - " + message.author);
    } else {
		var level = await(async function(){
			try{
				const member = message.guild.member(message.mentions.users.first());
				if(!member){
					const iUser = await sql.get(`SELECT * FROM userScores WHERE guildID = '${message.guild.id}' AND userID = '${member? member.id : message.author.id}'`); //gets user row of whos requesting
					if(!iUser){
						return 1;
					}else{
						return iUser.uLevel;
					}
				}
			} catch (e){
				console.log("Exception occured", e);
				return 1;
			}
		}(message));
		if(level && level < 1) {
			level = 1;
		}
		console.log("Level is:: "+level);
		const weightedRandom = function(level){
			var result = 0;
			for (let i=0; i<=level; i++){
				result = Math.max(Math.random(), result);
			}
			return result;
		}
        var fs = require("fs");
        var alturarose = fs.readFileSync("alturamegan.txt", "utf8");
        var hipModifier = parseFloat(fs.readFileSync("hipModifier.txt", "utf8"));
        var numberalturarose = Number(alturarose);
         var adicion = [3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 8, 8, 8, 8, 8 , 10, 10, 10, 15, 44, 100];
		var hipBonus = Math.random() < 0.075;
        if (hipBonus) {
            hipModifier += 0.0025;
        }
        var adicionrandom = Math.floor(weightedRandom(level) * adicion.length);
        var adiciontotal = adicion[adicionrandom];
        var alturanuevarose = numberalturarose + adiciontotal;
        var bustorose = Math.round((alturanuevarose * 0.4)) / 100;
        var cinturarose = Math.round((alturanuevarose * 0.35)) / 100;
        var caderarose = Math.round((alturanuevarose * hipModifier)) / 100;
        var bustoantiguorose = Math.round((alturarose * 0.4)) / 100;
        var cinturaantiguarose = Math.round((alturarose * 0.35)) / 100;
        var caderaantiguarose = Math.round((alturarose * hipModifier)) / 100;
        var pesobustorose = Math.round(bustorose * 10);
        var weight = Math.round(20.4 * (alturanuevarose/100) * (alturanuevarose/100));
        var alturaroseenmetros1 = numberalturarose / 100;
        var alturaroseenmetros2 = alturanuevarose / 100;
		var weight = Math.round(alturanuevarose * alturanuevarose * alturanuevarose * 0.000001091);
        var role = message.guild.roles.find(role => role.name === "Growth master");
        fs.writeFile("alturamegan.txt", alturanuevarose, function(err) {
            if (err) return console.log(err);
        });
        fs.writeFile("hipModifier.txt", hipModifier, function(err) {
            if (err) return console.log(err);
        });
       var facts = [
            "Big in Japan!!!",
            "Get off my way! I need to see Jack now",
            "Who gave you that damn serum? It's... for a friend",
            "Gigantify!",
            "<<<<STOP!!!",
            "This feels weird...",
            "It went to my hips, isn't it?",
            "Don't make me step on you!",
            "Am I bigger than rose?",
            "I loved that jeans...",
            message.author + " STOP!!!"
        ];
        var fact = Math.floor(Math.random() * facts.length);
        var embed = new Discord.RichEmbed()
            .setTitle(alturaroseenmetros1 + " Meters" + "->" + alturaroseenmetros2 + " Meters")
            .setDescription(facts[fact])
            .setColor(0xff00d6)
            .addField("**Bust =**", bustoantiguorose + " Meters" + "->" + bustorose + " Meters")
            .addField( "**Waist =**", cinturaantiguarose + " Meters" + "->" + cinturarose + " Meters")
            .addField("**Hips=**", caderaantiguarose + " Meters" + "->" + caderarose + " Meters")
            .addField("**Weight=**", weight +" KG")
            .setThumbnail("https://i.imgur.com/oaS7Y6g.png");
        message.channel.send({
            embed
        });
        if (adiciontotal > 99) {
            message.member.addRole(role);
            message.channel.send(message.author + "  You made me a monster! I'm one meter taller now T_T");
        }
		if(hipBonus) {
            message.channel.send(message.author + "  Now where am I going to get a belt that fits? T_T");
		}
	    growthCooldown[message.author.id] = new Date().getTime() + 14400000;
    }
};